import typer
from sqlalchemy import create_engine
from sqlalchemy.engine import Engine

from app.db.base_class import Base
from app.settings import settings


def init_db(db_url: str = settings.SQLALCHEMY_DATABASE_URI) -> Engine:
    engine = create_engine(
        db_url,
        connect_args={
            'check_same_thread': False,
        },
    )
    Base.metadata.create_all(bind=engine)
    return engine


if __name__ == '__main__':
    typer.run(init_db)
